import React, { useState } from "react";
import AccountData from "./components/AccountSection/AccountData";
import Footer from "./components/Footer";
import Menu from "./components/Menu/Menu";
import TasksSection from "./components/TasksSection/TasksSection";
import ModalCreateTask from "./components/Utilities/ModalTask";
import { Task } from "./interfaces";
import { useAppDispatch, useAppSelector } from "./store/hooks";
import { modalActions } from "./store/Modal.store";
import { tasksActions } from "./store/Tasks.store";
import ModalFeature from "./components/Utilities/ModalFeature";

const App: React.FC = () => {
  const modal = useAppSelector((state) => state.modal);
  const modalFeatureInit = () => {
    let currentState = sessionStorage.getItem("modalFeatureWasShown");
    return currentState === "false" || currentState === null || currentState === "";
  }
  const [modalFeatureIsShown, setModalFeatureIsShown] = useState<boolean>(modalFeatureInit());

  const dispatch = useAppDispatch();

  const closeModalCreateTask = () => {
    dispatch(modalActions.closeModalCreateTask());
  };

  const createNewTaskHandler = (task: Task) => {
    dispatch(tasksActions.addNewTask(task));
  };

  const closeFeatureModal = () => {
    setModalFeatureIsShown(false);
    sessionStorage.setItem("modalFeatureWasShown", "true");
  }

  return (
    <div className="bg-slate-200 min-h-screen text-slate-600 dark:bg-slate-900 dark:text-slate-400 xl:text-base sm:text-sm text-xs">
      {modal.modalCreateTaskOpen && (
        <ModalCreateTask
          onClose={closeModalCreateTask}
          nameForm="Add a task"
          onConfirm={createNewTaskHandler}
        />
      )}
      {modalFeatureIsShown && (
        <ModalFeature onClose={closeFeatureModal}/>
      )}
      <Menu />
      <TasksSection />
      <Footer />
      <AccountData />
    </div>
  );
};

export default App;
