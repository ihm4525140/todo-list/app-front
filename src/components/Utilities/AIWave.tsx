import React from "react";
import ReactSiriwave from "react-siriwave";

interface AIWaveProps {
    classes?: string;

}

const AIWave: React.FC<AIWaveProps> = ({ classes="hide-element" }) => {
    return (
        <div style={{maxWidth: 1, maxHeight: 1, justifyContent: "center"}} className={classes}>
            <ReactSiriwave width={350} theme="ios9" speed={0.1} amplitude={1.2} />
        </div>
    );
}

export default AIWave;