import React from "react";
import { ApiConsumer } from "../../service/ApiConsumer";

const HelpForm: React.FC<{ className?: string }> = ({ className }) => {
    const onFormSubmit = () => {

        const form = document.getElementById("formHelp") as HTMLFormElement;
        const input = form.querySelector("input") as HTMLInputElement;
        const value = input.value;
        input.value = "";
        if (value.trim().length === 0) {
          return;
        }
        ApiConsumer.routesGPTRequest(value);
    };
    return (
        <>
        <form style={{display: "flex", flexDirection: "column", justifyContent: "center"}} id="formHelp">
          <input className="inputStyles w-full mt-5" 
                  type="search"
                  placeholder="Where do you want to go?"
          />
          <button type="button" className={`btn  ${className}`} onClick={onFormSubmit}>Go to page</button>
        </form>
        </>
    );
};

export default HelpForm;