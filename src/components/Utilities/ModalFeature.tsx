import Modal from "./Modal";
import aiLogo from "../../assets/aiLogo.png";

const ModalFeature: React.FC<{
    onClose: () => void;
  }> = ({ onClose }) => {
    return (
        <Modal onClose={onClose} title={"Introducing Voice Mode."}>
            <div style={{display:"flex", alignItems: "center"}}>
                <img src={aiLogo} width={150} height={150} alt="Voice Mode" />
                <p className="mx-4">Move between pages, add new tasks, and more — <b>using only your voice</b>.</p>
            </div>
            
            <button className="btn mt-4" onClick={onClose}>Got it!</button>
        </Modal>
    );
}
export default ModalFeature;