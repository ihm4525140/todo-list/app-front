import React, { useState } from "react";
import { ApiConsumer } from "../../service/ApiConsumer";
import { AudioRecorder, useAudioRecorder } from 'react-audio-voice-recorder';
import AIWave from "./AIWave";
import { Task } from "../../interfaces";
import { tasksActions } from "../../store/Tasks.store";
import { useAppDispatch } from "../../store/hooks";

const BtnVoiceMode: React.FC<{ className?: string, prompt?: string }> = ({ className, prompt }) => {

  const recorderControls = useAudioRecorder();
  const [waveClass, setWaveClass] = useState("hide-element");
  const dispatch = useAppDispatch();

  const listen = () => {
    let startButton = document.getElementById("startListeningBtn");
    let stopButton = document.getElementById("stopListeningBtn");
    setWaveClass("d-flex");
    startButton?.style.setProperty("display", "none");
    stopButton?.style.setProperty("display", "block");
    recorderControls.startRecording();
  }

  const stopListening = () => {
    let startButton = document.getElementById("startListeningBtn");
    let stopButton = document.getElementById("stopListeningBtn");
    setWaveClass("hide-element");
    startButton?.style.setProperty("display", "block");
    stopButton?.style.setProperty("display", "none");
    recorderControls.stopRecording();
  }

  const addAudioElement = async (blob: any) => {
    const transcription = await ApiConsumer.genericWhisperRequest(blob);
    Promise.resolve(transcription).then((value) => {
      const task = ApiConsumer.genericGPTRequest(value.text);
      task.then((value) => {
        if (value != null && value != undefined) {
            value.id = Date.now().toLocaleString();
            dispatch(tasksActions.addNewTask(value!))
        }
      });
    });
  };

  return (
      <>
      <AudioRecorder
        onRecordingComplete={(blob) => addAudioElement(blob)}
        recorderControls={recorderControls}
        audioTrackConstraints={{
            echoCancellation: true,
            noiseSuppression: true,
            autoGainControl: true
          }}
        downloadFileExtension="webm"
        classes={{AudioRecorderClass: "hide-element"}}
        />
        <AIWave classes={waveClass}/>
        <button id="startListeningBtn" onClick={listen} >
          <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" className="bi bi-mic-fill" viewBox="0 0 16 16">
          <path d="M5 3a3 3 0 0 1 6 0v5a3 3 0 0 1-6 0z"/>
          <path d="M3.5 6.5A.5.5 0 0 1 4 7v1a4 4 0 0 0 8 0V7a.5.5 0 0 1 1 0v1a5 5 0 0 1-4.5 4.975V15h3a.5.5 0 0 1 0 1h-7a.5.5 0 0 1 0-1h3v-2.025A5 5 0 0 1 3 8V7a.5.5 0 0 1 .5-.5"/>
          </svg>
        </button>
        <button id="stopListeningBtn" style={{display:'none'}} onClick={stopListening} >
          <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" className="bi bi-stop-circle-fill" viewBox="0 0 16 16">
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M6.5 5A1.5 1.5 0 0 0 5 6.5v3A1.5 1.5 0 0 0 6.5 11h3A1.5 1.5 0 0 0 11 9.5v-3A1.5 1.5 0 0 0 9.5 5z"/>
          </svg>
        </button>
    
      </>
  );
};

export default BtnVoiceMode;