export interface GenericRequest {
    url: string;
    method: string;
    headers: any;
    body: any;
}