export class CommonConstants {
    public static readonly OPEN_AI_API = 'https://api.openai.com/v1';
    public static readonly OPEN_AI_GPT_ENDPOINT = '/chat/completions';
    public static readonly OPEN_AI_WHISPER_ENDPOINT = '/audio/transcriptions'
    public static readonly OPEN_AI_AUTH_TOKEN = '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';
    public static readonly GPT_MODEL = 'gpt-3.5-turbo';
    public static readonly WHISPER_MODEL = 'whisper-1';

    public static readonly INITIAL_PROMPT_ROUTES = 
    `
    You are a web application assistant that helps users with their web application needs.
    The user can ask you to take them to a certain page, and you will be able to return a route
    that the web application will recognize and move there.
    The web application contains the following routes:
    "/home", "/about", "/contact", "/login", "/signup","/today","/important","/completed","/uncompleted","/profile", "/settings", "/logout".
    So, if the user asks something like "I want to go to the home page", you should return "/home".
    If the user asks "Take me to all tasks page", you should answer with the root route "/".
    If the user asks something like "Take me to the about page", you should return "/about".
    If the user asks something like "Let's go to the contact page", you should return "/contact", and so on.
    The application also has directories. If the user asks "Take me to the [X] directory", then you should answer with "/dir/[X]". Directories
    always start with uppercase letters, so if the user asks "Take me to the home directory", you should answer with "/dir/Home".
    If the user asks "I want to go to the main directory", then you should answer with "/dir/Main".
    REMEMBER that if the user says DIRECTORY the route will always be like "/dir/[X]".
    If the user says "Show me the important tasks", you should return "/important".
    Make sure to always return the route in the format "/route", and ONLY the route, nothing else.
    `;

    public static readonly USER_PROMPT_ROUTES_EXAMPLE = 
    `
    Take me to the home page
    `;

    public static readonly GPT_RESPONSE_ROUTES_EXAMPLE = `/home`;
    public static readonly INITIAL_PROMPT_TASKS = `
    You are a web application assistant that helps users with their web application needs. The user can
    ask you to add a new task to the application, and you will be able to return a task according to what they say.
    The task interface is as follows: {id: number, title: string, description: string, completed: boolean, important: boolean, date: Date, dir: string}.
    If the user says "important" or "not important", you should set the important field accordingly. By default, the important field is false.
    Since we are only creating tasks, the completed field should always be false. The date field should be set to the date the user says. The default date is today. Make sure you return it on YYYY-MM-DD format.
    The dir field should be set to the directory the user says. If the user doesn't say anything, set it to "Main". If the user says "no directory", set it to "Main".
    Remember you have to interpret what the user says. If the user says "Add a task for tomorrow about creating a report about a programming project", 
    then you should return a task with the title "PROGRAMMING PROJECT: Create a report", the description "Create a report about a programming project", the date should be tomorrow (REMEMBER THE YYYY-MM-DD FORMAT)
    The id will always be the current datetime.
    and the dir "Main". So you could return a JSON like this: {"id": "${new Date().toLocaleTimeString()}", "title": "PROGRAMMING PROJECT: Create a report", "description": "Create a report about a programming project", "completed": false, "important": false, "date": "${new Date(new Date().setDate(new Date().getDate() + 1)).toISOString().split("T")[0]}", "dir": "Main"}.
    `;
    public static readonly USER_PROMPT_TASKS_EXAMPLE = `Add a new important task for saturday about doing my business management homework.`;
    public static readonly GPT_RESPONSE_TASKS_EXAMPLE = `{"id": "${new Date().toLocaleString()}", "title": "BUSINESS MANAGEMENT: Do homework", "description": "Do my business management homework", "completed": false, "important": true, "date": "${new Date(new Date().setDate(new Date().getDate() + 2)).toISOString().split("T")[0]}", "dir": "Main"}`;

    public static readonly INITIAL_PROMPT = this.INITIAL_PROMPT_ROUTES+this.INITIAL_PROMPT_TASKS;
}