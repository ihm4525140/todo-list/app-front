import { CommonConstants } from "../cons/CommonConstants";
import { GenericRequest } from "../interface/GenericRequest";
import OpenAI from 'openai';
import { Task } from '../interfaces';

export class ApiConsumer {

    

    public static async consumeApi(request: GenericRequest): Promise<any> {
        let response = await fetch(request.url, {
            method: request.method,
            headers: request.headers,
            body: request.body
        });
        return await response.json();
    }

    public static async routesGPTRequest(prompt: string) {
      const openai = new OpenAI({apiKey: CommonConstants.OPEN_AI_AUTH_TOKEN, dangerouslyAllowBrowser: true});
      const response = await openai.chat.completions.create({
        messages: [
          {
            "role":"system",
            "content": CommonConstants.INITIAL_PROMPT_ROUTES
          },
          {
            "role":"user",
            "content": CommonConstants.USER_PROMPT_ROUTES_EXAMPLE
          },
          {
            "role":"assistant",
            "content": CommonConstants.GPT_RESPONSE_ROUTES_EXAMPLE
          },
          {
            "role":"user",
            "content": prompt
          } 
        ],
        model: CommonConstants.GPT_MODEL
      });
      const url = response.choices[0].message.content;
      if (url != null) {
        window.location.href = url;
      }
    }

    public static async genericWhisperRequest(blob: Blob) {
      const openai = new OpenAI({apiKey: CommonConstants.OPEN_AI_AUTH_TOKEN, dangerouslyAllowBrowser: true});
      const transcription = await openai.audio.transcriptions.create({
        file: new File([blob], "recording.webm", { type: "audio/webm" }),
        model: CommonConstants.WHISPER_MODEL
      });
      return transcription;
    }

    public static async genericGPTRequest(prompt: string) {
      const openai = new OpenAI({apiKey: CommonConstants.OPEN_AI_AUTH_TOKEN, dangerouslyAllowBrowser: true});
      const response = await openai.chat.completions.create({
        messages: [
          {
            "role":"system",
            "content": CommonConstants.INITIAL_PROMPT
          },
          {
            "role":"user",
            "content": CommonConstants.USER_PROMPT_ROUTES_EXAMPLE
          },
          {
            "role":"assistant",
            "content": CommonConstants.GPT_RESPONSE_ROUTES_EXAMPLE
          },
          {
            "role":"user",
            "content": CommonConstants.USER_PROMPT_TASKS_EXAMPLE
          },
          {
            "role":"assistant",
            "content": CommonConstants.GPT_RESPONSE_TASKS_EXAMPLE
          },
          {
            "role":"user",
            "content": prompt
          } 
        ],
        model: CommonConstants.GPT_MODEL
      });
      const resp = response.choices[0].message.content;
      if (resp?.includes("{")) {
        return JSON.parse(resp) as Task;
      }
      else {
        console.log(resp);
        window.location.href = resp != null ? resp : "";
      }
    }

}